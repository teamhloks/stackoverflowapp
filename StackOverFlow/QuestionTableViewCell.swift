//
//  QuestionTableViewCell.swift
//  StackOverFlow
//
//  Created by Lehlohonolo Mbele on 2021/03/07.
//

import UIKit

class QuestionTableViewCell: UITableViewCell {

    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
  
    
    public func configure(withQuestion question: String, name: String) {
        questionLabel.text = question
        nameLabel.text = name
    }
    
}
