//
//  Question.swift
//  StackOverFlow
//
//  Created by Lehlohonolo Mbele on 2021/03/07.
//

import Foundation

struct Tags: Codable {
    let tags: [String]
}

struct Owner: Codable {
    let display_name: String
}

struct Item: Codable {
    let title: String
    let tags: [String]
    let owner: Owner
}

struct Items: Codable {
    let items: [Item]
}
