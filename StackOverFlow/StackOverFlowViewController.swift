//
//  ViewController.swift
//  StackOverFlow
//
//  Created by Lehlohonolo Mbele on 2021/03/07.
//

import UIKit

class StackOverFlowViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private let viewModel = StackOverFlowViewModel(stackOverFlowService: StackOverFlowServiceImplementation())
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "QuestionTableViewCell", bundle: nil), forCellReuseIdentifier: "QuestionTableViewCell")
        viewModel.fetchQuestions { [weak self] error in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
    }
    
    
}

extension StackOverFlowViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionTableViewCell", for: indexPath) as? QuestionTableViewCell else {
            return UITableViewCell()
        }
        let question = viewModel.question(inRow: indexPath.row)
        cell.nameLabel.text = question?.owner.display_name
        cell.questionLabel.text = question?.title
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
}

