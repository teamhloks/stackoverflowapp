//
//  StackOverFlowService.swift
//  StackOverFlow
//
//  Created by Lehlohonolo Mbele on 2021/03/07.
//

import Foundation

protocol StackOverFlowService {
    func fetchQuestions(completionHandler: @escaping ([Item]?, Error?) -> Void?)
}


class StackOverFlowServiceImplementation: StackOverFlowService {
    let apiEndpoint = "https://api.stackexchange.com/2.2/questions?order=desc&sort=activity&site=stackoverflow"
    
    func fetchQuestions(completionHandler: @escaping ([Item]?, Error?) -> Void?) {
        if let url = URL(string: apiEndpoint) {
            print(url)
            let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                if error != nil || data == nil {
                    completionHandler(nil, NSError())
                } else {
                    if let response = String(data: data!, encoding: .utf8) {
                        if let jsonData = response.data(using: .utf8) {
                            let questions = try! JSONDecoder().decode(Items.self, from: jsonData)
                            completionHandler(questions.items, nil)
                        }
                    }
                }
            }
            task.resume()
        }
    }
    
    
    
}
