//
//  StackOverFlowViewModel.swift
//  StackOverFlow
//
//  Created by Lehlohonolo Mbele on 2021/03/07.
//

import Foundation

class StackOverFlowViewModel {
    let stackOverFlowService : StackOverFlowService
    var questions: [Item]?
    
    var numberOfRows: Int {
        return questions?.count ?? 0
    }
    
    init(stackOverFlowService: StackOverFlowService) {
        self.stackOverFlowService = stackOverFlowService
    }
    
    func fetchQuestions(completionHandler: @escaping (Error?) -> Void?){
        stackOverFlowService.fetchQuestions { (questions, error) -> Void? in
            if error != nil {
                completionHandler(error)
            } else {
                self.questions = questions
                completionHandler(nil)
            }
            return nil
        }
    }
    
    func question(inRow row: Int) -> Item? {
        return questions?[row]
    }
    
}
